(def p (os/spawn (string/split " " "fc-list : file family charset")
                 :p
                 {:out :pipe}))

(def out (ev/read (p :out) :all))

(defn match-until [s] ~(sequence (capture (to ,s)) ,s))

(def rule "match line of output from fc-list"
  ~(*
    ,(match-until ": ")
    ,(match-until ":charset=")
    (capture (any 1))))

(defn process-range [s]
  (def parsed (map |(scan-number $ 16) (string/split "-" s)))
  (if (= 1 (length parsed))
      (parsed 0)
      (tuple (splice parsed))))

(defn process-line [match]
  (def [a b c] match)
  {:filename a
   :fullnames (string/split "," b)
   :ranges (map process-range (string/split " " c))})

(def fonts (->> (string/split "\n" out)
                (filter (fn [line] (not (empty? line))))
                (map (fn [line] (process-line (peg/match rule line))))))

(pp fonts)

