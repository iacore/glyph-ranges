#!/usr/bin/env python

import sys, os, re, subprocess, typing


class FontInfo(typing.NamedTuple):
    path: str
    names: list[str]
    ranges: list[(int, int) or int]


def parse_range(token: str) -> (int, int) or int:
    if "-" in token:
        a, b = token.split("-")
        return (int(a, 16), int(b, 16))
    else:
        return int(token, 16)


def parse_line(line: str) -> FontInfo:
    match = re.match("(.*): (.*):charset=(.*)", line)
    assert match

    ranges = list(map(parse_range, match[3].split()))

    return FontInfo(match[1], match[2].split(","), ranges)


def get_fonts() -> list[FontInfo]:
    p = subprocess.run(
        "fc-list : file family charset".split(" "), stdout=subprocess.PIPE
    )
    lines = p.stdout.decode().splitlines()
    return list(map(parse_line, lines))

try:
    output_type = sys.argv[1]
except:
    output_type = 'raw'

fonts = get_fonts()

if output_type == 'raw':
    for font in fonts:
        print(font)
elif output_type == 'json':
    import json
    json.dump(fonts, sys.stdout)
else:
    raise ValueError('Output type must be "raw" or "json"')